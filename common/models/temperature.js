'use strict';

module.exports = function(Temperature) {

  Temperature.average = function(cb) {
    Temperature.find({fields: {value: true, date: true}}, function(err, temperatures) {
      if (err) {
        console.log(err);
        cb(null, err);
      } else if (temperatures.length == 0)
        cb(null, 'N/A');
      else {
        let sum = 0;
        for (let temperature of temperatures) {
          sum += temperature.value;
        }
        let average = sum / temperatures.length;
        cb(null, average);
      }
    });
  };

  Temperature.median = function(cb) {
    Temperature.find({fields: {value: true, date: true}, order: 'value ASC'}, function(err, temperatures) {
      if (err) {
        console.log(err);
        cb(null, err);
      } else if (temperatures.length == 0)
        cb(null, 'N/A');
      else if (temperatures.length % 2 != 0) {
        let median = temperatures[Math.trunc(temperatures.length/2)].value;
        cb(null, median);
      } else {
        let middle = temperatures.length / 2;
        let median = (temperatures[middle - 1].value + temperatures[middle].value) / 2;
        cb(null, median);
      }
    });
  };

  Temperature.min = function(cb) {
    Temperature.find({fields: {value: true, date: true},  order: 'value ASC', limit: 1}, function(err, temperatures) {
      console.log(temperatures);
      if (err) {
        console.log(err);
        cb(null, err);
      } else if (temperatures.length == 0)
        cb(null, 'N/A');
      else {
        cb(null, temperatures[0].value);
      }
    });
  };

  Temperature.max = function(cb) {
    Temperature.find({fields: {value: true, date: true}, order: 'value DESC', limit: 1}, function(err, temperatures) {
      if (err) {
        console.log(err);
        cb(null, err);
      } else if (temperatures.length == 0)
        cb(null, 'N/A');
      else {
        cb(null, temperatures[0].value);
      }
    });
  };

  Temperature.remoteMethod(
    'average',
    {
      http: {path: '/average', verb: 'get'},
      returns: {arg: 'value', type: 'number'},
    }
  );

  Temperature.remoteMethod(
    'median',
    {
      http: {path: '/median', verb: 'get'},
      returns: {arg: 'value', type: 'number'},
    }
  );

  Temperature.remoteMethod(
    'min',
    {
      http: {path: '/min', verb: 'get'},
      returns: {arg: 'value', type: 'number'},
    }
  );

  Temperature.remoteMethod(
    'max',
    {
      http: {path: '/max', verb: 'get'},
      returns: {arg: 'value', type: 'number'},
    }
  );

};
