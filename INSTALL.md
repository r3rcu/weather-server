1. Inside this folder execute "npm install"
2. Create a database in postgresql (with any name) and configure the connection parameters in "server/datasources.json"
3. Execute "npm ." the server will be running through port 3000.
4. The documentation of the rest api is in "http: //localhost:3000/explorer/"
